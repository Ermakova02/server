

import java.io.*;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;


public class FTServerSocket implements Runnable {
    private static final int SPEED_UPDATE_DELAY = 3000;

    private Thread t;
    private Socket socket;
    private int connectionNumber;
    private long instantSpeed;
    private long averageSpeed;
    private Timer timer;

    FTServerSocket(Socket socket, int connectionNumber) {
        this.socket = socket;
        this.connectionNumber = connectionNumber;
        timer = new Timer(true);
        timer.schedule(new SpeedUpdateTimer(), SPEED_UPDATE_DELAY, SPEED_UPDATE_DELAY);
        t = new Thread(this, "FTServerSocket " + String.valueOf(connectionNumber));
        t.start();
    }

    private static String formatSpeed(long speed) {
        if (speed < 1024) return new String(String.valueOf(speed) + " bytes per second");
        speed /= 1024;
        if (speed < 1024 ) return new String(String.valueOf(speed) + " Kbytes per second");
        speed /= 1024;
        if (speed < 1024 ) return new String(String.valueOf(speed) + " Mbytes per second");
        speed /= 1024;
        if (speed < 1024 ) return new String(String.valueOf(speed) + " Gbytes per second");
        speed /= 1024;
        return new String(String.valueOf(speed) + " Tbytes per second");
    }

    private synchronized void setInstantSpeed(long speed) { instantSpeed = speed; }

    private synchronized void setAverageSpeed(long speed) { averageSpeed = speed; }

    private synchronized long getInstantSpeed() { return instantSpeed; }

    private synchronized long getAverageSpeed() { return averageSpeed; }

    private void closeSocket() {
        try {
            if (socket != null)
                socket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        InputStream sin  = null;
        OutputStream sout = null;
        try {
            sin = socket.getInputStream();
            sout = socket.getOutputStream();
            DataInputStream dis = new DataInputStream (sin);
            String fileName = dis.readUTF();
            System.out.println("Client " + String.valueOf(connectionNumber) + ": upload file " + fileName);
            long fileSize = dis.readLong();
            int packetSize = dis.readInt();
            byte buf[] = new byte[packetSize];
            String uploadFileName = Server.UPLOADS_DIR_NAME + File.separator + fileName;
            File file = new File(uploadFileName);
            boolean fileOperation = false;
            if (file.exists()) {
                fileOperation = file.delete();
                if (!fileOperation) {
                    closeSocket();
                    return;
                }
            }
            fileOperation = file.createNewFile();
            if (!fileOperation) {
                closeSocket();
                return;
            }
            FileOutputStream fos = new FileOutputStream(file);
            BufferedInputStream bis = new BufferedInputStream(sin);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            int bytesRead = 0;
            long totalBytesRead = 0;
            long startTime = System.currentTimeMillis();
            long prevTime = startTime;
            long currTime = 0;
            while ((bytesRead = bis.read(buf, 0, packetSize)) != -1){
                bos.write(buf, 0, bytesRead);
                totalBytesRead += bytesRead;
                if (totalBytesRead == fileSize) break;
                currTime = System.currentTimeMillis();
                if (currTime - prevTime != 0)
                    setInstantSpeed((long)bytesRead * 1000 / (currTime - prevTime));
                if (currTime - startTime != 0)
                    setAverageSpeed(totalBytesRead * 1000 / (currTime - startTime));
                prevTime = currTime;
            }
            timer.cancel();
            boolean result = fileSize == totalBytesRead;
            sout.write(result ? 1 : 0);
            bis.close();
            bos.close();
            fos.close();
            dis.close();
            sin.close();
            sout.close();
            closeSocket();
            System.out.println("Client " + String.valueOf(connectionNumber) + ": upload file finished");
        } catch (IOException e) {
            e.printStackTrace();
            timer.cancel();
            closeSocket();
            return;
        }
    }

    class SpeedUpdateTimer extends TimerTask {

        @Override
        public void run() {
            System.out.println("Client " + String.valueOf(connectionNumber) + ":");
            System.out.println("Instant speed: " + formatSpeed(getInstantSpeed()));
            System.out.println("Average speed: " + formatSpeed(getAverageSpeed()));
            System.out.println("");
        }
    }
}
