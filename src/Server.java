import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public final static String UPLOADS_DIR_NAME = "uploads";
    private int port;

    private void CreateUploadsDir() {
        File theDir = new File(UPLOADS_DIR_NAME);
        if (!theDir.exists()) {
            try{
                theDir.mkdir();
            }
            catch(SecurityException ex){
                ex.printStackTrace();
                System.exit(0);
            }
        }
    }

    Server(int port) {
        this.port = port;
        CreateUploadsDir();
    }

    public void start() {
        int connectionNumber = 0;
        ServerSocket srvSocket = null;
        try {
            srvSocket = new ServerSocket(port);
        }
        catch (IOException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        while(true) {
            Socket socket = null;
            try {
                // Waiting for new connection
                socket = srvSocket.accept();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
            // Start work with client in new thread
            new FTServerSocket(socket, connectionNumber++);
        }
    }
}
